var express = require('express');
var fs = require('fs');
var bodyParser = require("body-parser");
var jsonParser = bodyParser.json();
var router = express.Router();

//const { getName } = require("../services/user.service");
//const { saveName } = require("../services/user.service");
//const { getNameById } = require("../services/user.service");
const { deleteUser } = require("../services/user.service");
const { updateUser } = require("../services/user.service");
const { getUserById } = require("../services/user.service");
const { addNewUser } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

router.get('/user', isAuthorized, function(req, res) {
  var content = fs.readFileSync("users.json", "utf8");
  var users = JSON.parse(content);
  if(users) res.send(users);
  else {
    res.status(400).send(`User list is empty`);
  }
});

router.get('/user/:id', isAuthorized, function(req, res) {
  let id = req.params.id; 
  let content = fs.readFileSync("users.json", "utf8");
  let users = JSON.parse(content);
  const result = getUserById(users, id);
  if (result) {
    res.send(result);
  }
  else{
      res.status(404).send("Error with finding user by id");
  }
});

router.post('/user', isAuthorized, jsonParser, function(req, res) {
  if(req.body){
    const name = req.body.name;
    const health = req.body.health;
    const attack = req.body.attack;
    const defense = req.body.defense;
    let user = {name: name, health: health, attack:attack, defense: defense};
    const result = addNewUser(users, user);
    if (result){
      res.send(result);
    }
    else {
      res.status(400).send("Error is happened when new user has being added to storage");
    }

  }
  else {
    return res.status(400).send('Empty data is getted in creating new user function');
  }
});

router.put('/user/:id', isAuthorized, jsonParser, function(req, res) {
  if(req.body){
    const idUser = req.body.id;
    const name = req.body.name;
    const health = req.body.health;
    const attack = req.body.attack;
    const defense = req.body.defense;

    let updatedUser = {name: name, health: health, attack:attack, defense: defense};
    const result = updateUser(users, idUser, updatedUser);
    if (result){
      res.send(result);
    }
    else {
      res.status(404).send("Error is happened when user has being updated in storage");
    }

  }
  else {
    return res.status(404).send('Oh my Gosh! I found an error! User is upsent in storage');
  }
});
router.delete('user/:id', isAuthorized, function(req, res) {
  const idUser = req.param.id;
  const idUnneadedUser = getUserById(users, idUser);
  if (idUnneadedUser >-1) {
    const result = deleteUser(users, idUser);
    if(result){
      res.send(result);
    }
    else{
      res.status(400).send("Error is happened when user has being deleted in storage");
    }
  }
  else {
    res.status(404).send("User is upsent in storage");
  }
});
module.exports = router;
