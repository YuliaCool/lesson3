const { saveData } = require("../repositories/user.repository");

const getName = (user) => {
    if (user) {
      return user.name;
    } else {
      return null;
    }
  };
  
  const getNameById = (users, id) => {
    if(users){
        for(let i = 0; i<users.legth; i++){
            if(users[i].id === id) {
                user = users[i];
                break;
            }
          }
          return user.name;
    }
    else {
        return null;
    }
  }

  const getUserById = (users, id) => {
    if(users){
        for(let i = 0; i<users.legth; i++){
            if(users[i].id === id){
                user = users[i];
                break;
            }
        }
        return user;
    }
    else {
        return null;
    }
  }


  const addNewUser = (users, newUser) => {
    const id = Math.max.apply(Math, users.map(function(i){
     return i.id;
    }));
    user.id = id++;
    users.push(newUser);
    let finalData = JSON.stringify(users);
    fs.writeFileSync("users.json", finalData);
    return users;
  }

  const updateUser = (users, idUser, updatedUser) => {
    let oldUser = getUserById(users, idUser);
    for (property in oldUser) {
        if(oldUser[property] != updatedUser[property])
            oldUser[property] = updatedUser[property];
    }
     let finalData = JSON.stringify(users);
     fs.writeFileSync("users.json", finalData);
     return users;
   }
  
   const deleteUser = (users, idUser) => {
       const updatedUsersList = users.splice(idUser, 1)[0];
       var date = JSON.stringify(updatedUsersList);
        fs.writeFileSync("users.json", finalData);
        return users;
   }

 /*const saveName = (user) => {
    if (user) {
      return saveData(user.name); /// Я так понимаю, что данная функция (она описана в user.repository.js)
      должна использоваться в случае, если бы у нас была реальная база данных, но так как мы используем
      её иммитацию и запись в файл, то этот слой с репозиторием мы можем пропустить.
    } else {
      return null;
    }
 };*/

  module.exports = {
    getName,
    getNameById,
    getUserById,
    addNewUser,
    updateUser,
    deleteUser
  };